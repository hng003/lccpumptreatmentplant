﻿using System.Windows;
using PTP.View;


namespace PTP
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var v = new MainWindow();
            v.Show();
        }
    }
}
