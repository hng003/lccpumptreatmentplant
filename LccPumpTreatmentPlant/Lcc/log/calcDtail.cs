﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using demo.Lcc.plant;
using System.IO;

namespace demo.Lcc.log
{
    class calcDtail
    {
        class FormDtail
        {
            public long Year { get; set; }
            public long Nonber { get; set; }
            public string Name1 { get; set; }
            public string Name2 { get; set; }
            public string Name3 { get; set; }
            public string Name4 { get; set; }
            public string Name { get; set; }
            public string Place { get; set; } //場所
            public long LinkCount { get; set; }//修繕連動数
            public long Linkage1 { get; set; } //連動1
            public long Linkage2 { get; set; } //連動2
            public long Linkage3 { get; set; } //連動3
            public long Linkage4 { get; set; } //連動4
            public long Linkage5 { get; set; } //連動5
            public long Install { get; set; }
            public long StandardYear { get; set; }
            public long TargetYear { get; set; }
            public long InstYear { get; set; }
            public double Servicelife { get; set; }          
            public double DegYear { get; set; } //経過年
            public double SoundnessValue { get; set; }//健全度
            public long Soundness { get; set; }//健全度ランク
            public double Probability { get; set; }//発生確率
            public int ProbabilityRank { get; set; }//発生確率ランク
            public double Surface { get; set; }//影響度
            public int SurfaceRank { get; set; }//影響度ランク
            public int RiskRank { get; set; }//リスク評価
            public long Priority { get; set; }//機器友禅順位
            public bool IsTarget { get; set; }//修繕予定判定
            public bool IsNextTo { get; set; }//次年繰越判定
            public bool IsExecu { get; set; }//修繕実施判定  
            public string SortRank { get; set; }//修繕順位
            public long SortNo { get; set; }//修繕番号
            public long UpdateYear { get; set; }//改築期間
            public decimal MacCost { get; set; }
            public decimal RepCost { get; set; }
            public double NextDegYear { get; set; } //修繕後 経過年
            public double NextSoundnessValue { get; set; }//修繕後 健全度
            public long NextSoundness { get; set; }//修繕後 健全度ランク

            public FormDtail()
            {
                Year = 0;
                Nonber = 0;
                Name1 = "";
                Name2 = "";
                Name3 = "";
                Name4 = "";
                Name = "";
                Place = "";
                LinkCount = 0;
                Linkage1 = 0;
                Linkage2 = 0;
                Linkage3 = 0;
                Linkage4 = 0;
                Linkage5 = 0;
                Install = 0;
                StandardYear = 0;
                TargetYear = 0;
                InstYear = 0;
                Servicelife = 0;
                DegYear = 0;
                SoundnessValue = 0;
                Soundness = 0;
                Probability = 0;
                ProbabilityRank = 0;
                Surface = 0;
                SurfaceRank = 0;
                RiskRank = 0;
                Priority = 0;
                IsTarget = false;
                IsNextTo = false;
                IsExecu = false;
                SortRank = "";
                SortNo = 0;
                UpdateYear = 0;
                MacCost = 0;
                RepCost = 0;
                NextDegYear = 0;
                NextSoundnessValue = 0;
                NextSoundness = 0;
            }

            public static string header()
            {
                string d = "";
                d = d + "計算年" + ",";//Year
                d = d + "資産番号" + ",";//Nonber
                d = d + "工種" + ",";//Name1
                d = d + "大分類" + ",";//Name2
                d = d + "中分類" + ",";//Name3
                d = d + "小分類" + ",";//Name4
                d = d + "機器名称" + ",";//Name
                d = d + "修繕費用" + ",";//Place
                d = d + "連動数" + ",";//LinkCount
                d = d + "連動①" + ",";//Linkage1
                d = d + "連動②" + ",";//Linkage2
                d = d + "連動③" + ",";//Linkage3
                d = d + "連動④" + ",";//Linkage4
                d = d + "連動⑤" + ",";//Linkage5
                d = d + "設置年度" + ",";//Install
                d = d + "標準耐用年数" + ",";//StandardYear
                d = d + "目標耐用年数" + ",";//TargetYear
                d = d + "計算耐用年数" + ",";//Servicelife
                d = d + "経過年" + ","; //DegYear         
                d = d + "健全度値" + ",";//SoundnessValue
                d = d + "健全度" + ",";//Soundness
                d = d + "発生確率" + ",";//Probability
                d = d + "発生確率ランク" + ",";//ProbabilityRank
                d = d + "影響度" + ",";//Surface
                d = d + "影響度ランク" + ",";//SurfaceRank
                d = d + "リスク評価" + ",";//RiskRank
                d = d + "改築期間" + ",";//UpdateYear
                d = d + "機器優先順位" + ",";//Priority
                d = d + "修繕予定判定" + ",";//IsTarget
                d = d + "次年繰越判定" + ",";//IsNextTo
                d = d + "修繕実施判定" + ","; //IsExecu
                d = d + "修繕優先順位" + ",";//SortRank
                d = d + "修繕実施番号" + ",";//SortNo
                d = d + "修繕機器費用" + ",";//MacCost
                d = d + "修繕費用" + ",";//RepCost
                d = d + "次年経過年" + ",";//NextDegYear
                d = d + "次年健全度値" + ",";//NextSoundnessValue
                d = d + "次年健全度";//NextSoundness
                return d;
            }

            public string text() {
                string d = "";
                d = d + Year + ",";
                d = d + Nonber + ",";
                d = d + Name1 + ",";
                d = d + Name2 + ",";
                d = d + Name3 + ",";
                d = d + Name4 + ",";
                d = d + Name + ",";
                d = d + Place + ",";
                d = d + LinkCount + ",";
                d = d + Linkage1 + ",";
                d = d + Linkage2 + ",";
                d = d + Linkage3 + ",";
                d = d + Linkage4 + ",";
                d = d + Linkage5 + ",";
                d = d + Install + ",";
                d = d + StandardYear + ",";
                d = d + TargetYear + ",";
                d = d + Servicelife + ",";
                d = d + DegYear + ",";           
                d = d + SoundnessValue + ",";
                d = d + Soundness + ",";
                d = d + Probability + ",";
                d = d + ProbabilityRank + ",";
                d = d + Surface + ",";
                d = d + SurfaceRank + ",";
                d = d + RiskRank + ",";
                d = d + UpdateYear + ",";
                d = d + Priority + ",";
                d = d + IsTarget + ",";
                d = d + IsNextTo + ",";
                d = d + IsExecu + ",";
                d = d + SortRank + ",";
                d = d + SortNo + ",";
                d = d + MacCost + ",";
                d = d + RepCost + ",";
                d = d + NextDegYear + ",";
                d = d + NextSoundnessValue + ",";
                d = d + NextSoundness;
                return d;
            }
        }

        SortedList<long, SortedList<long, FormDtail>> DtailList;

        public calcDtail() {
            DtailList = new SortedList<long, SortedList<long, FormDtail>>();
        }

        public void Add(long year, SortedList<long, MachineClass> mlist) {
            SortedList<long, FormDtail> log = new SortedList<long, FormDtail>();
            long i = 0;
            foreach (MachineClass d in mlist.Values)
            {
                FormDtail f = new FormDtail();
                f.Year = year;
                f.Nonber = d.AssetNo;
                f.Name1 = d.Name1;
                f.Name2 = d.Name2;
                f.Name3 = d.Name3;
                f.Name4 = d.Name4;
                f.Name = d.Name;
                f.Place = d.Place;
                f.Linkage1 = d.Repair.Link.GetListNo(1);
                f.Linkage2 = d.Repair.Link.GetListNo(2);
                f.Linkage3 = d.Repair.Link.GetListNo(3);
                f.Linkage4 = d.Repair.Link.GetListNo(4);
                f.Linkage5 = d.Repair.Link.GetListNo(5);
                f.Install = d.Install;
                f.StandardYear = d.StandardYear;
                f.TargetYear = d.TargetYear;
                f.Servicelife = d.Deg.ServiceLife;
                f.DegYear = d.Deg.BeforeYear;             
                f.SoundnessValue = d.Deg.BeforeSoundnessValue;
                f.Soundness = d.Deg.BeforeSoundness;
                f.Probability = d.Occ.Rate;
                f.ProbabilityRank = d.Occ.EvaluationRank();
                f.Surface = d.Imp.rate;
                f.SurfaceRank = d.Imp.EvaluationRank();
                f.RiskRank = d.Risk.Evaluation;
                f.Priority = d.Repair.Priority;
                f.IsTarget = d.Repair.IsTarget;
                f.IsNextTo = d.Repair.IsNextTo;
                f.IsExecu = d.Repair.IsExecu;
                f.SortRank = d.Repair.SortRank;
                f.SortNo = d.Repair.SortNo;
                f.UpdateYear = d.Repair.UpdateYear;
                f.LinkCount = d.Repair.Link.GetList.Count;
                f.MacCost = d.Repair.Cost;
                f.RepCost = d.RepCost;
                f.NextDegYear = d.Deg.Year;
                f.NextSoundnessValue = d.Deg.SoundnessValue;
                f.NextSoundness = d.Deg.Soundness;
                log.Add(i++, f);
            }
            DtailList.Add(year, log);
            
        }

        public void ExportCsv(){

            try
            {
                StreamWriter sw = new StreamWriter("calcdtail.csv", false, Encoding.UTF8);
                sw.WriteLine(string.Join(",", FormDtail.header()));
                long  s = DtailList.Keys[0];//start year
                long  e = DtailList.Keys[DtailList.Count - 1];//end year
                for (long i = s;  e >= i; i++)
                {
                    SortedList<long, FormDtail> log = DtailList[i];
                    foreach (FormDtail d in log.Values)
                    {
                        sw.WriteLine(string.Join(",", d.text()));
                    }
                }
                sw.Dispose();
            }
            catch (Exception e)
            {
                string msg = e.Message;
            }

        }

        
    }
}
