﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo.Lcc.deg
{
    class DegAge
    {
        private static double maxSoundness = 5;//健全度係数：5段階評価    
        public double BeforeYear { get; set; }//当初年次
        public double Year { get;  set; }//現状年次
        public double ServiceLife { get; set; }//耐用年数
        public long UpdateWork { get; set; }//更新期間

        public DegAge() {
            ServiceLife = 0;
            UpdateWork = 0;
        }

        // Cloneメゾット
        public DegAge Clone()
        {
            return (DegAge)MemberwiseClone();
        }
        public void LccInit() {
            BeforeYear = Year;//set calcYear
        }
        public void Advance() {
            Year++;
        }
        public void Recovery()
        {
            Year = 1;
            UpdateWork--;
        }
        public double BeforeSoundnessValue
        {
            get
            {
                return calcSoundness(BeforeYear);
            }
        }
        public long BeforeSoundness
        {
            get
            {
                return SoundnessRank(BeforeSoundnessValue);
            }
        }
        public double SoundnessValue
        {
            get
            {
                return calcSoundness(Year);
            }
        }

        public long Soundness
        {
            get
            {               
                return SoundnessRank(SoundnessValue);
            }
        }
        private double calcSoundness(double value)
        {
            return (maxSoundness * 10 - ((maxSoundness - 2) / ServiceLife * 10) * value) / 10;
        }
        private long SoundnessRank(double value) {
            //[良い]5→4→3→2→1[悪]
            long result = 0;
            if (value > 4.0)
            {
                result = 5;
            }
            else if (value > 3.0)
            {
                result = 4;
            }
            else if (value > 2.0)
            {
                result = 3;
            }
            else if (value > 1.0)
            {
                result = 2;
            }
            else
            {
                result = 1;
            }
            return result;
        }
        public bool IsLimit() {
            if (ServiceLife <= Year) { return true; }
            return false;
        }
        public long GetUpdateWork
        {
            get
            {
                return UpdateWork;
            }
        }

    }
}
