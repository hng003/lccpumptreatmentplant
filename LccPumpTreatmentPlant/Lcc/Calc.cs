﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using demo.Lcc.com;
using demo.Lcc.deg;
using demo.Lcc.plant;
using demo.Lcc.rep;
using demo.Lcc.log;
using demo.Lcc.xlsx;

namespace demo.lcc
{    
    class Calc : IDisposable
    {
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Release any managed resources here.
            }
            // Release any unmanaged resources not wrapped by safe handles here.
        }

        ~Calc()
        {
            Dispose(false);
        }

        Common com;
        SortedList<string, lndustrialSpecies> facility;
        SortedList<long, MachineClass> calcList;
        calcDtail logDtail;

        public Calc() {
            facility = new SortedList<string, lndustrialSpecies>();
            calcList = new SortedList<long, MachineClass>();
            logDtail = new calcDtail();
        }

        public void Builder() {
            CreateClass();
            SortedList<long, MachineClass> cloneList = CloneList();
            SortedList<string, long> repairList = new SortedList<string, long>();

            logDtail.Add(com.lccStartYear - 1, cloneList);
            for (long i = com.lccStartYear; i  < com.lccEndYear; i++) {
                LccInit(cloneList, repairList);
                Preprocess1(cloneList, repairList);//1.修繕判定
                Preprocess2(cloneList, repairList);//2.修繕実施
                Preprocess3(cloneList);//3.経年劣化

                logDtail.Add(i, cloneList);
            }
            logDtail.ExportCsv();
        }

        //計算クラスインスタンス
        private void CreateClass()
        {
            //1.common create
            com = new Common();
            com.Set();

            //2.class create
            xlsxRead read = new xlsxRead();
            SortedList<long, string[]> ledger = read.Ledger();//Ledger
            foreach (string[] d in ledger.Values)
            {
                lndustrialSpecies cls1 = new lndustrialSpecies(d);
                LargeClass cls2 = new LargeClass(d);
                MiddleClass cls3 = new MiddleClass(d);
                SmallClass cls4 = new SmallClass(d);
                MachineClass mac = new MachineClass(d, com);
                setFacility(cls1, cls2, cls3, cls4, mac);//add facility
                setCalcList(mac);//add calclist
            }
        }

        private void LccInit(SortedList<long, MachineClass> list, SortedList<string, long> rlist) {
            rlist.Clear();
            com.Budget.setYearCost(com.lccStartYear);           
            foreach (MachineClass d in list.Values)
            {
                d.LccInit();
            }
        }
        private void Preprocess1(SortedList<long, MachineClass> list, SortedList<string, long> rlist)
        {
            //setp1// is repair target add
            SortedList<long, long> interim = new SortedList<long, long>();
            foreach (MachineClass d in list.Values)
            {
                d.Repair.Priority = d.Priority;//add priority
                if (d.Repair.IsRepair())
                {
                    d.RepCost = setRepair(d);//repair add
                    interim.Add(d.Repair.Priority, d.AssetNo);
                }
            }
            //setp2// link add           
            foreach (long i in interim.Values)//loop list repair
            {               
                MachineClass d = list[i];
                d.RepCost = d.RepCost + setLinkRepair(d, list);
            }
            //setp3// sort list add
            foreach (MachineClass d in list.Values)
            {
                if (d.Repair.IsTarget)
                {
                    rlist.Add(d.Repair.SortRank, d.AssetNo);
                }
            }
        }        
        private void Preprocess2(SortedList<long, MachineClass> list, SortedList<string, long> rlist)
        {
            long sno = 1;
            foreach (long i in rlist.Values)//loop list repair
            {
                MachineClass d = list[i];
                d.Repair.SortNo = sno++;//sortNo++
                if (d.Repair.IsTarget)
                {
                    d.Repair.Totalcost = d.Repair.Totalcost + d.Repair.Cost;
                    if (d.Repair.Budget.isLimit())
                    {
                        d.Repair.Execution();
                    }                 
                }
                else {
                    d.Repair.Deg.Advance();
                }
            }
        }
        private void Preprocess3(SortedList<long, MachineClass> list)
        {
            foreach (MachineClass d in list.Values)//loop list repair
            {              
                if (!d.Repair.IsExecu)
                {
                    d.Repair.Deg.Advance();
                }              
            }
        }

        // add facility
        private void setFacility(lndustrialSpecies class1, LargeClass class2, MiddleClass class3, SmallClass class4, MachineClass mac)
        {
            if (!facility.ContainsKey(class1.Name))
            {
                class1.Add(class2, class3, class4, mac);
                facility.Add(class1.Name, class1);
            }
            else
            {
                class1 = facility[class1.Name];
                class1.Add(class2, class3, class4, mac);
            }
        }
        // add calclist
        private void setCalcList(MachineClass mac) {
            long i = calcList.Count;
            calcList.Add(i++, mac);
        }

        private SortedList<long, MachineClass> CloneList()
        {
            SortedList<long, MachineClass> clonelist = new SortedList<long, MachineClass>();
            foreach (MachineClass d in calcList.Values)
            {
                MachineClass c = d.Clone();
                c.Deg = d.Deg.Clone();
                c.Risk = d.Risk.Clone();
                c.Risk.Occ.DegAge = c.Deg;
                c.Repair = d.Repair.Clone();
                c.Repair.Deg = c.Deg;
                c.Repair.Resk = c.Risk;
                clonelist.Add(c.AssetNo, c);
            }
            return clonelist;
        }

        private decimal setRepair(MachineClass d)
        {
            d.Repair.IsTarget = true;
            d.Repair.setUpdateWork();//period
            d.Repair.SortRank = SortRankCode(d.Repair.SortUpdateWork, d.Repair.Priority, d.Repair.LinkNo, d.AssetNo); //sort rank 
            return d.Repair.Cost;
        }
        private decimal setLinkRepair(MachineClass d, SortedList<long, MachineClass> list)
        {
            decimal cost = 0;
            if (d.Repair.Link.GetCount == 0 || d.Repair.LinkAssetNo > 0) { return cost; }

            long MaxPriority = getLinkMaxPriority(d, list);//get max priority                       
            d.Repair.LinkAssetNo = d.AssetNo;
            d.Repair.SortRank = SortRankCode(d.Repair.SortUpdateWork, MaxPriority, d.Repair.LinkNo, d.AssetNo); //sort rank  

            long i = 1;
            foreach (long l in d.Repair.Link.GetList.Values)
            {
                MachineClass x = list[l];
                if (x.Repair.LinkAssetNo == 0) {
                    x.Repair.LinkAssetNo = d.AssetNo;
                    x.Repair.LinkNo = i++;
                    x.RepCost = 0;
                    cost = cost + setRepair(x);
                    x.Repair.SortRank = SortRankCode(d.Repair.SortUpdateWork, MaxPriority, x.Repair.LinkNo, x.AssetNo); //sort rank update
                }             
            }
            d.Repair.Link.Cost = cost;
            return cost;
        }
        private long getLinkMaxPriority(MachineClass d, SortedList<long, MachineClass> list)
        {          
            long maxPriority =  d.Repair.Priority;
            foreach (long l in d.Repair.Link.GetList.Values)
            {
                MachineClass x = list[l];
                if (x.Repair.LinkAssetNo == 0 && x.Priority < maxPriority)
                {
                        maxPriority = x.Repair.Priority;
                }
            }
            return maxPriority;
        }
        /// <summary>
        /// 
        /// create sort rank code
        /// 
        /// </summary>
        /// <param name="sortUpdateWork"></param>
        /// <param name="priority"></param>
        /// <param name="linkNo"></param>
        /// <param name="assetNo"></param>
        /// <returns></returns>
        private string SortRankCode(string sortUpdateWork, long priority,  long linkNo, long assetNo) {
            return "x" + sortUpdateWork + priority + linkNo + assetNo;//sort rank update
        }
    }
}
