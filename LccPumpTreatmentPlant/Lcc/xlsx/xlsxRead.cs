﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo.Lcc.xlsx
{
    class xlsxRead
    {   public SortedList<long, String[]> Common()
        {
            using (xlsxIo xls = new xlsxIo()) { return xls.Read("lcc_common.xlsx", "condition", 0);  }
            //return new xlsxIo().Read("lcc_common.xlsx", "condition", 0);
        }
        public SortedList<long, String[]> Ledger() {
            return new xlsxIo().Read("lcc_ledger.xlsx", "base", 2); 
        }
        public SortedList<long, String[]> ImpactRank()
        {
            return new xlsxIo().Read("lcc_riskRanks.xlsx", "影響度", 0);
        }
        public SortedList<long, String[]> OccurrenceRank()
        {
            return new xlsxIo().Read("lcc_riskRanks.xlsx", "発生確率", 0);
        }
        public SortedList<long, String[]> RiskRank()
        {
            return new xlsxIo().Read("lcc_riskRanks.xlsx", "リスク評価", 0);
        }
    }
}
