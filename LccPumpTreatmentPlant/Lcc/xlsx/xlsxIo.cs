﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;

namespace demo.Lcc.xlsx
{
    class xlsxIo : IDisposable
    {     
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Release any managed resources here.
            }
            // Release any unmanaged resources not wrapped by safe handles here.

        }

        ~xlsxIo()
        {
            Dispose(false);
        }

        public SortedList<long, String[]> Read(string filepath, string sheetname, int headerrow)
        {
            SortedList<long, String[]> xlsx = new SortedList<long, String[]>();           
            try
            {
                IWorkbook workbook = WorkbookFactory.Create(filepath);
                ISheet worksheet = workbook.GetSheet(sheetname);
                int lastRow = worksheet.LastRowNum;
                for (int i = headerrow; i <= lastRow; i++)
                {
                    IRow row = worksheet.GetRow(i);
                    String[] val = new String[row.Cells.Count];
                    for (int j = 0; j <= row.Cells.Count - 1; j++)
                    {
                        val[j] = StringCellValue(row.Cells[j]);
                    }
                    xlsx.Add(i, val);
                }
                workbook.Close();
            }
            catch (Exception e) {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            finally {               
            }

                return xlsx;
        }

        private String StringCellValue(ICell cell) {
            string cellStr = string.Empty;
            switch (cell.CellType)
            {                
                case CellType.String://文字列型
                    cellStr = cell.StringCellValue;
                    break;
               
                case CellType.Numeric: // 数値型（日付の場合もここに入る）                   
                    if (DateUtil.IsCellDateFormatted(cell)) // セルが日付情報が単なる数値かを判定
                    {
                        cellStr = cell.DateCellValue.ToString("yyyy/MM/dd HH:mm:ss");// 日付型
                    }
                    else
                    {                       
                        cellStr = cell.NumericCellValue.ToString(); // 数値型
                    }
                    break;
                case CellType.Boolean:
                    cellStr = cell.BooleanCellValue.ToString(); //bool型(文字列でTrueとか入れておけばbool型として扱われた)
                    break;
                case CellType.Blank:// 入力なし
                    cellStr = cell.ToString();
                    break;                
                case CellType.Formula:// 数式
                    // 下記で数式の文字列が取得される
                    //cellStr = cell.CellFormula.ToString();
                    // 数式の元となったセルの型を取得して同様の処理を行う
                    // コメントは省略
                    switch (cell.CachedFormulaResultType)
                    {
                        case CellType.String:
                            cellStr = cell.StringCellValue;
                            break;
                        case CellType.Numeric:

                            if (DateUtil.IsCellDateFormatted(cell))
                            {
                                cellStr = cell.DateCellValue.ToString("yyyy/MM/dd HH:mm:ss");
                            }
                            else
                            {
                                cellStr = cell.NumericCellValue.ToString();
                            }
                            break;
                        case CellType.Boolean:
                            cellStr = cell.BooleanCellValue.ToString();
                            break;
                        case CellType.Blank:
                            break;
                        case CellType.Error:
                            cellStr = cell.ErrorCellValue.ToString();
                            break;
                        case CellType.Unknown:
                            break;
                        default:
                            break;
                    }
                    break;
                case CellType.Error: // エラー
                    cellStr = cell.ErrorCellValue.ToString();
                    break;
                case CellType.Unknown:  // 型不明なセル
                    break;
                default: // もっと不明なセル（あぶない刑事をなぜか思い出しました）
                    break;
            }
            return cellStr;
        }
    }
}
