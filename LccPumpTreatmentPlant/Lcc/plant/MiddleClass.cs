﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo.Lcc.plant
{
    class MiddleClass
    {
        private SortedList<String, SmallClass> smallclass = new SortedList<String, SmallClass>();
        public String Name { get; set; }
        public SortedList<String, SmallClass> List
        {
            get { return smallclass; }
            set { smallclass = value; }
        }
        public MiddleClass(String[] x)
        {
            Name = x[4];
        }       
        public void Add(SmallClass class1, MachineClass mac)
        {
            if (!List.ContainsKey(class1.Name))
            {
                class1.Add(mac);
                List.Add(class1.Name, class1);
            }
            else
            {
                SmallClass existingList = List[class1.Name];
                existingList.Add(mac);
            }
        }

    }
}