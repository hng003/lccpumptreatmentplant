﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo.Lcc.plant
{
    class lndustrialSpecies
    {
        private SortedList<string, LargeClass> lageclass = new SortedList<string, LargeClass>();
        public SortedList<string, LargeClass> List
        {
            get { return lageclass; }
            set { lageclass = value; }
        }
        public string Name { get; set; }
        public lndustrialSpecies(string[] x) {
            Name = x[2];
        }        
        public void Add(LargeClass c1, MiddleClass c2, SmallClass c3, MachineClass c4)
        {
            if (!List.ContainsKey(c1.Name))
            {
                c1.Add(c2, c3, c4);
                List.Add(c1.Name, c1);
            }
            else
            {
                LargeClass existingList = List[c1.Name];
                existingList.Add(c2, c3, c4);
            }
        }
    }
}
