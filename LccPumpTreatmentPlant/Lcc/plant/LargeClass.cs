﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo.Lcc.plant
{
    class LargeClass
    {
        private SortedList<string, MiddleClass> middleclass = new SortedList<string, MiddleClass>();
        public string Name { get; set; }
        public SortedList<string, MiddleClass> List
        {
            get { return middleclass; }
            set { middleclass = value; }
        }
        public LargeClass(string[] x)
        {
            Name = x[3];
        }     
        public void Add(MiddleClass class1, SmallClass class2, MachineClass mac)
        {          
            if (!List.ContainsKey(class1.Name))
            {
                class1.Add(class2, mac);
                List.Add(class1.Name, class1);
            }
            else
            {
                MiddleClass existingList = List[class1.Name];
                existingList.Add(class2, mac);
            }
        }
    }
}
