﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using demo.Lcc.com;
using demo.Lcc.deg;
using demo.Lcc.eval;
using demo.Lcc.rep;

namespace demo.Lcc.plant
{
    class MachineClass
    {        public long AssetNo { get; set; }//資産番号
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string Name4 { get; set; }
        public string Name { get; set; }
        public long Install { get; set; }
        public long StandardYear { get; set; }
        public long TargetYear { get; set; }
        public long Year { get; set; }
        public long Servicelife { get; set; }
        public string Place { get; set; }
               
        public DegAge Deg { get; set; }
        public Occurrence Occ { get; set; }
        public Impact Imp { get; set; }
        public Risk Risk { get; set; }
        public Repair Repair { get; set; }
        public decimal RepCost { get; set; }

        // Cloneメゾット
        public MachineClass Clone()
        {          
            return (MachineClass)MemberwiseClone();
        }
    

        public MachineClass(string[] x, Common com)
        {
            link_degl(com);
            link_eval(com);

            AssetNo = long.Parse(x[1]);
            Name1 = x[2];
            Name2 = x[3];
            Name3 = x[4];
            Name4 = x[5];
            Name = x[6];
            Install = long.Parse(x[7]);
            StandardYear = long.Parse(x[9]);
            TargetYear = long.Parse(x[10]);
            Deg.BeforeYear = (com.lccStartYear - 1) - Install;//前年で設定
            Deg.Year =　com.lccStartYear - Install;//start年で設定
            Deg.ServiceLife = long.Parse(x[10]);
            Imp.Surface_f = double.Parse(x[13]);
            Imp.Surface_c = double.Parse(x[14]);
            Imp.Surface_i = double.Parse(x[15]);
            Imp.Surface = double.Parse(x[16]);
            Place = x[17];
            Repair.Cost = decimal.Parse(x[18]);
            Repair.UpdateYear = long.Parse(x[19]);
            Repair.Link.Add(x[20], x[21], x[22], x[23], x[24]);
        }
        public void LccInit()
        {
            Deg.LccInit();
            Repair.LccInit();
            Repair.Budget.LccInit();
        }

        private void link_degl(Common com)
        {
            Deg = new DegAge
            {
            };
        }

        private void link_eval(Common com)
        {
            Occ = new Occurrence
            {
                DegAge = Deg,
                Rank = com.occurrRanks
            };
            Imp = new Impact
            {                
                Rank = com.impactRanks
            };
            Risk = new Risk
            {
                Occ = Occ,
                Imp = Imp,
                Rank = com.riskRanks
            };
            Repair = new Repair
            {
                Budget = com.Budget,             
                Deg = Deg,
                Resk = Risk
            };
            Repair.Link = new LinkMachine
            {
            };
        }       
        public long Priority//機器優先順位
        {
            get
            {
                string code = "";
                code = code + Deg.Soundness.ToString().PadLeft(1, '0');//1.健全度[1桁]
                code = code + Risk.SortNo().ToString().PadLeft(2, '0');//2.リスク評価[2桁
                code = code + AssetNo.ToString().PadLeft(10, '0');//3.資産番号[10桁]]    
                return long.Parse(code);
            }
        }
      

    }
}
