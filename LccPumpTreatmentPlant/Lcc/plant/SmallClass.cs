﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo.Lcc.plant
{
    class SmallClass
    {
        private SortedList<long, MachineClass> machineclass = new SortedList<long, MachineClass>();
        public String Name { get; set; }
        public SmallClass(String[] x)
        {
            Name = x[5];
        }
        public SortedList<long, MachineClass> List
        {
            get { return machineclass; }
            set { machineclass = value; }
        }       
        public void Add(MachineClass mac)
        {
            List.Add(mac.AssetNo, mac);
        }
    }
}
