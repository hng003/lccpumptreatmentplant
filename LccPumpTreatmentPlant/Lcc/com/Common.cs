﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using demo.Lcc.xlsx;
using demo.Lcc.eval;
using demo.Lcc.rep;

namespace demo.Lcc.com
{
    class Common
    {
        public long lccStartYear { get; set; }
        public long lccEndYear { get; set; }
        public long lccPeriod { get; set; }
        public int isUsefulLife { get; set; }//更新期間
        public int isUnder { get; set; }//0:予算無繰越, 1:予算超繰越

        public Budget Budget { get; set; }//予算
        public ImpactRanks impactRanks { get; set; }//影響度
        public OccurrenceRanks occurrRanks { get; set; }//発生確率
        public RiskRanks riskRanks { get; set; }//リスク評価

        public Common() {
            lccStartYear = 0;
            lccEndYear = 0;
            lccPeriod = 0;
            isUsefulLife = 0;
            isUnder = 0;
            Budget = new Budget();
            impactRanks = new ImpactRanks();
            occurrRanks = new OccurrenceRanks();
            riskRanks = new RiskRanks();            
        }

        public string Set() {
            string errorString = "";
            try {
                xlsxRead read = new xlsxRead();
                SortedList<long, string[]> com = read.Common();
                setPeriodYear(com);
                setUsefulLife(com);
                Budget.Set(com);
                impactRanks.Set(read.ImpactRank());
                occurrRanks.Set(read.OccurrenceRank());
                riskRanks.Set(read.RiskRank());
            }
            catch (Exception e) {
                errorString = e.Message;
            }
            return errorString;
        }
        //計算期間
        private void setPeriodYear(SortedList<long, string[]> com) {
            string[] d = com.Values[0];
            lccStartYear = long.Parse(d[1]);
            lccEndYear = long.Parse(d[2]);
            lccPeriod = long.Parse(d[3]);
        }
        //改築期間[0:標準耐用 1:目標耐用]
        private void setUsefulLife(SortedList<long, string[]> com)
        {
            string[] d = com.Values[1];
            isUsefulLife = int.Parse(d[1]);
        }       
    }
}
