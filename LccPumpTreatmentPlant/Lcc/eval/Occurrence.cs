﻿// created by docon on 2017/12/00.
//発生確率

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using demo.Lcc.deg;

namespace demo.Lcc.eval
{
    class Occurrence
    {
        public DegAge DegAge { get; set; }
        public OccurrenceRanks Rank { get; set; }

        public double BeforerRate
        {
            get
            {
                return Math.Round((double)DegAge .BeforeYear/ (double)DegAge.ServiceLife, 2);
            }
        }
        public double Rate
        {
            get
            {
                return Math.Round((double)DegAge.Year / (double)DegAge.ServiceLife,2);
            }
        }
        // Cloneメゾット
        public Occurrence Clone()
        {
            return (Occurrence)MemberwiseClone();
        }
        public int BeforerEvaluationRank()
        {
            return Rank.getRank(BeforerRate);
        }
        public int EvaluationRank() {            
            return Rank.getRank(Rate);
        }
    }
}
