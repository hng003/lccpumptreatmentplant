﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo.Lcc.eval
{
    class ImpactRanks
    {
        class RankValue {
           public int Rank { get; set; }
            public  double ToValue { get; set; }
            public  double FormValue { get; set; }
        }
        
        SortedList<int, RankValue> list { get; set; }


        // Cloneメゾット
        public ImpactRanks Clone()
        {
            return (ImpactRanks)MemberwiseClone();
        }

        public ImpactRanks() {
            list = new SortedList<int, RankValue>();            
        }
        public void Set(SortedList<long, string[]> list)
        {
            int headers = 2;
            string[] x = list.Values[0];
            int count = int.Parse(x[1]) + headers;
            for (int i = headers; i < count; i++)
            {
                string[] d = list.Values[i];
                Add(d);
            }
        }
        private void Add(String[] x) {
            RankValue d = new RankValue();
            d.Rank = int.Parse(x[0]);
            d.FormValue = double.Parse(x[1]);
            d.ToValue = double.Parse(x[2]);
            list.Add(d.Rank, d);
        }

        public int getRank(double value) {
            RankValue min = list[1];
            RankValue max = list[list.Count];
            if (value < min.FormValue) { return list[0].Rank; }//以下
            if (value > max.ToValue) { return list[list.Count].Rank; }//以上
            foreach (RankValue d in list.Values)//loop list repair
            {
                if (d.FormValue <= value && d.ToValue >= value) {
                    return d.Rank;
                }
            }
            return 0;
        }

    }
}
