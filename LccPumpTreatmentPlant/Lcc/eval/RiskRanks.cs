﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo.Lcc.eval
{
    class RiskRanks
    {
        public int MatrixCount { set; get; }
        SortedList<int,  int> list { get; set; }

        // Cloneメゾット
        public RiskRanks Clone()
        {
            return (RiskRanks)MemberwiseClone();
        }
        public RiskRanks() {
            MatrixCount = 0;
            list = new SortedList<int, int>();
        }
        public void Set(SortedList<long, string[]> list)
        {
            string[] r = list.Values[0];
            int rowcount = int.Parse(r[3]);
            string[] c = list.Values[1];
            int colcount = int.Parse(c[3]);

            MatrixCount = rowcount * colcount;

            int occRank = 1;
            for (int i = 4; i < rowcount; i++)
            {
                int impRank = 1;
                string[] d = list.Values[i];
                for (int j = 2; j < colcount + 2; j++)
                {
                    Add(occRank, impRank, int.Parse(d[j]));
                    impRank++;
                }
                occRank++;
            }
        }
        private void Add(int x, int y, int rank)
        {
            list.Add(key(x, y), rank);
        }
        public int getRank(int x, int y)
        {
            if (list.ContainsKey(key(x, y))) {
                return list[key(x, y)];
            }
            return 0;
        }

        private int key(int x, int y) {
            return x * 10 + y;
        }

    }
}
