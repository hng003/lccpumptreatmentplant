﻿// created by docon on 2017/12/00.
//リスク評価

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo.Lcc.eval
{
    class Risk
    {
        public Occurrence Occ { get; set; }
        public Impact Imp { get; set; }
        public RiskRanks Rank { get; set; }
        public int BeforeEvaluation
        {
            get
            {
                return Rank.getRank(Imp.EvaluationRank(), Occ.BeforerEvaluationRank());
            }
        }     
        public int Evaluation
        {
            get
            {
                return Rank.getRank(Imp.EvaluationRank(), Occ.EvaluationRank());
            }
        }
        public int BeforeSortNo()
        {
            return calcSortNo(BeforeEvaluation);
        }
        public int SortNo()
        {                     
            return calcSortNo(Evaluation);      
        }
        // Cloneメゾット
        public Risk Clone()
        {
            Occ.Clone();
            Imp.Clone();
            return (Risk)MemberwiseClone();
        }
        public int calcSortNo(int evaluation) {
            return Rank.MatrixCount - evaluation;
        }
    }
}
