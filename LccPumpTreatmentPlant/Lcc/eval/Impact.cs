﻿// created by docon on 2017/12/00.
//影響度（被害規模）

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using demo.Lcc.deg;

namespace demo.Lcc.eval
{
    class Impact
    {  
        public ImpactRanks Rank { get; set; }
        public double Surface_f { get; set; }
        public double Surface_c { get; set; }
        public double Surface_i { get; set; }
        public double Surface { get; set; }
    
        // Cloneメゾット
        public Impact Clone()
        {
            return (Impact)MemberwiseClone();
        }
        public double rate
        {
            get
            {
                return Surface;
            }
        }
        public int EvaluationRank()
        {
            return Rank.getRank(rate);
        }

    }
}
