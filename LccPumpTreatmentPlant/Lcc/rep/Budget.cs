﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace demo.Lcc.rep
{
    class Budget
    {
        public decimal BudgetCost { get; set; }
        public decimal Totalcost { get; set; }
        private int IsUnder { get; set; }//0:予算内繰越 1:予算超繰越
        private bool IsNextUnder { get; set; }//繰越基準
        public SortedList<long, Decimal> budgetList { get; set; }//予算金額
        
        // Cloneメゾット
        public Budget Clone()
        {
            return (Budget)MemberwiseClone();
        }
        public Budget() {
            budgetList = new SortedList<long, Decimal>();
            IsUnder = 0;
            LccInit();
        }
        public void LccInit()
        {         
            IsNextUnder = false;//繰越基準
        }
        public void Set(SortedList<long, string[]> com)
        {
            string[] x = com.Values[0];
            long year = long.Parse(x[1]);//開始年次

            string[] d = com.Values[2];//予算金額
            for (long i = 1; i < d.Length; i++)
            {
                budgetList.Add(year, decimal.Parse(d[i]));
                year++;
            }

            string[] n = com.Values[3];
            IsUnder = int.Parse(n[1]);//予算判定[0:予算内繰越1: 予算超繰越]
        }
        public void setYearCost(long year) {
            BudgetCost = budgetList[year];
        }      
        public bool isLimit()
        {
            if (IsUnder == 0){//繰越基準            
                if (BudgetCost > Totalcost) { return true; }//予算内繰越
            }else {
                if (IsNextUnder == true) { return true; }//予算超繰越
                if (BudgetCost > Totalcost) { IsNextUnder = true; } 
            }
            return false;
        }


    }
}
