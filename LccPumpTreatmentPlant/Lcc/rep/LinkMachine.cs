﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using demo.Lcc.plant;

namespace demo.Lcc.rep
{
    class LinkMachine
    {
        SortedList<long, long> linkList;
        public decimal Cost { get; set; }

        public LinkMachine() {
            linkList = new SortedList<long, long>();
            Cost = 0;
        }
        public void Add(string x1, string x2, string x3, string x4, string x5) {
            if (long.Parse(x1) != 0) { linkList.Add(1, long.Parse(x1)); }
            if (long.Parse(x2) != 0) { linkList.Add(2, long.Parse(x2)); }
            if (long.Parse(x3) != 0) { linkList.Add(3, long.Parse(x3)); }
            if (long.Parse(x4) != 0) { linkList.Add(4, long.Parse(x4)); }
            if (long.Parse(x5) != 0) { linkList.Add(5, long.Parse(x5)); }
        }
        public SortedList<long, long> GetList
        {
            get
            {
                return linkList;
            }           
        }
        public long GetCount
        {
            get
            {
                return linkList.Count;
            }
        }
        public long GetListNo(long x)
        {
            long no = 0;
            if (linkList.ContainsKey(x)) { no = linkList[x]; }
            return no; 
        }        
    }
}
