﻿// created by docon on 2017/12/00.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using demo.Lcc.com;
using demo.Lcc.deg;
using demo.Lcc.eval;

namespace demo.Lcc.rep
{
    class Repair
    {       
        public Budget Budget { get; set; }
        public DegAge Deg { get; set; }
        public Risk Resk { get; set; }
        public Decimal Cost { get; set; }
        public LinkMachine Link { get; set; }        
        public bool IsTarget { get; set; }//修繕予定
        public bool IsNextTo { get; set; }//修繕繰越
        public bool IsExecu { get; set; }//修繕実行
        public long Priority { get; set; }//機器優先順位
        public string SortRank { get; set; }//修繕順位
        public long SortNo { get; set; }//優先番号
        public long UpdateYear { get; set; }//改築期間
        public long LinkAssetNo { get; set; }//連動元ランク
        public long LinkNo { get; set; }//連動番号

        // Cloneメゾット
        public Repair Clone()
        {
            return (Repair)MemberwiseClone();
        }      
        public Repair() {
            UpdateYear = 0;
            LccInit();
        }
        public void LccInit()
        {
            IsTarget = false;
            IsNextTo = false;
            IsExecu = false;
            Priority = 0;
            SortNo = 0;
            SortRank = "";
            LinkAssetNo = 0;
            LinkNo = 0;
        }
        public bool IsRepair()
        {
            if (Deg.IsLimit() && !IsTarget) { return true; }
            return false;
        }

        public decimal Execution() {
            IsExecu = true;
            Deg.Recovery();
            return Cost;
        }
        public void setUpdateWork()
        {
            Deg.UpdateWork = UpdateYear;
        }

        public decimal Totalcost
        {
            get
            {
                return Budget.Totalcost;
            }
            set
            {
                Budget.Totalcost = value;
            }
        }
        public String SortUpdateWork
        {
            get
            {
                if (UpdateYear ==  Deg.GetUpdateWork) {
                    return "9";
                }
                return Deg.GetUpdateWork.ToString().PadLeft(1, '0');
            }
        }
        public String SortLinkNo
        {
            get
            {
                return LinkNo.ToString().PadLeft(1, '0');
            }
        }

    }


}
