﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PTP.Common;
using PTP.Model;

namespace PTP.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private long startYear;

        private long endYear;

        private long period;

        private UpdatePreiodVewModel selectedUpdatePreiodType;

        public MainViewModel()
        {
            this.UpdatePeriodTypes = UpdatePreiodVewModel.Create();
                this.SelectedUpdatePreiodType = this.UpdatePeriodTypes.First();
        }

        public IEnumerable<UpdatePreiodVewModel> UpdatePeriodTypes { get; private set; }

        public UpdatePreiodVewModel SelectedUpdatePreiodType
        {
            get
            {
                return this.selectedUpdatePreiodType;
            }

            set
            {
                this.selectedUpdatePreiodType = value;
                this.RaisePropertyChanged("SelectedUpdatePreiodType");
            }
        }


    }
}
