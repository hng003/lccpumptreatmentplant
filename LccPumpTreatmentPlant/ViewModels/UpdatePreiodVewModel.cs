﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PTP.Common;
using PTP.Model;

namespace PTP.ViewModels
{
    public class UpdatePreiodVewModel : ViewModelBase
    {

        public UpdatePeriodType UpdatePeriodType { get; private set; }
        
        public string Label { get; private set; }

        public UpdatePreiodVewModel(UpdatePeriodType updateperiodType, string label)
        {
            this.UpdatePeriodType = updateperiodType;
            this.Label = label;
        }
             
        private static Dictionary<UpdatePeriodType, string> typeLabelMap = new Dictionary<UpdatePeriodType, string>
        {
            { UpdatePeriodType.None, "未選択" },
            { UpdatePeriodType.Standard, "標準" },
            { UpdatePeriodType.Goal, "目標" }
        };
               
        public static UpdatePreiodVewModel Create(UpdatePeriodType type)
        {
            return new UpdatePreiodVewModel(type, typeLabelMap[type]);
        }
      
        public static IEnumerable<UpdatePreiodVewModel> Create()
        {
            foreach (UpdatePeriodType e in Enum.GetValues(typeof(UpdatePeriodType)))
            {
                yield return Create(e);
            }
        }

    }
}
