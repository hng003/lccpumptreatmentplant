﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTP.Model
{
    public enum UpdatePeriodType
    {
        /// <summary>
        /// 更新期間選択
        /// </summary>
        None,
        /// <summary>
        /// 標準耐用年数
        /// </summary>
        Standard,
        /// <summary>
        /// 目標耐用年数
        /// </summary>
        Goal
    }

    class UpdatePeriod
    {
        //更新期間を表す番号
        private static readonly Dictionary<UpdatePeriodType, int> calcMap = new Dictionary<UpdatePeriodType, int>
        {
            // 未指定
            {
                UpdatePeriodType.None, 0
            },
            // 標準耐用年数
            {
                UpdatePeriodType.Standard, 1
            },
            // 目標耐用年数
            {
                UpdatePeriodType.Goal, 2
            }       
        };





    }
}
